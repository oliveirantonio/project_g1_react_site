import React, { Component } from 'react';


class Topic extends Component {
  render() {
    return (
        <div>
            <h1>Topic</h1>
            <ul>
                <li> URL: {this.props.match.url}</li>
                <li> PATH: {this.props.match.path}</li>
                <li> ID: {this.props.match.params.id}</li>
            </ul>
        </div>
    )
  }
}

export default Topic;