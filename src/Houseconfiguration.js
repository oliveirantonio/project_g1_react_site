import React, { Component } from 'react';
import {Link} from "react-router-dom";

class Houseconfiguration extends Component {
  render() {
    return(
        <div>
            <br></br>
        <div className="geral">
          <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <a href="#" id="bot-us">(Administrator) I want to add a new room to the house, in order to configure it(name, house floor and dimensions).</a>
                    </div>
                </div>
          </div>
        </div>

        <div className="geral">
            <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <a href="#" id="bot-us">(Administrator) I want to have a list of existing rooms, so that I can choose one to edit it.</a>
                    </div>
                </div>
            </div>
        </div>

        <div className="geral">
            <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <a href="#" id="bot-us">US109 (Administrator) I want to edit the configuration of an existing room, so that I can reconfigure it.</a>
                    </div>
                </div>
            </div>
        </div>

        <div className="geral">
            <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <a href="#" id="bot-us">(Administrator) I want to create a house grid, so that I can define the rooms that are attached to it and the contracted maximum power for that grid.</a>
                    </div>
                </div>
            </div>
        </div>

        <div className="geral">
            <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <a href="#" id="bot-us">(Administrator) I want to have a list of existing rooms attached to a house grid, so that I can attach/detach rooms from it.</a>
                    </div>
                </div>
            </div>
        </div>

        <div className="geral">
            <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <Link to="/us147"><a href="us147.html" id="bot-us">US147 (Administrator) I want to attach a room to a house grid, so that the room’s power and energy consumption is included in that grid.</a></Link>
                    </div>
                </div>
            </div>
        </div>

        <div className="geral">
            <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <a href="#" id="bot-us">US149 (Administrator) I want to detach a room from a house grid, so that the room’s power and energy consumption is not included in that grid. The room’s characteristics are not changed.</a>
                    </div>
                </div>
            </div>
        </div>

        <div className="geral">
            <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <a href="#" id="bot-us">(Administrator) I want to get a list of all sensors in a room, so that I can configure them.</a>
                    </div>
                </div>
            </div>
        </div>

        <div className="geral">
            <div className="container">
                <div className="row" id="center">
                    <div className="col-sm" id="border">
                        <a href="#" id="bot-us">(Administrator) I want to add a new sensor to a room from the list of available sensor types, in order to configure it.</a>
                    </div>
                </div>
            </div>
        </div>
            <br></br>
        </div>
    )
  }
}

export default Houseconfiguration;