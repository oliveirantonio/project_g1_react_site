import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Home from './Home';
import Houseconfiguration from './Houseconfiguration';
import Roomconfiguration from './Roomconfiguration';
import NoMatch from './NoMatch';
import Topics from './Topics';
import Topic from './Topic';
import Us147 from './Us147';

class App extends Component {



  render() {
    return (
      <div>
        <Router>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link to="/home"><a className="navbar-brand" href="#">SmartHome</a></Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                    aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
              <ul className="navbar-nav mr-auto" id="font-size-menu">
                <li className="nav-item">
                  <Link to="/houseconfiguration"><a className="nav-link" href="#">House Configuration</a></Link>
                </li>
                <li className="nav-item">
                  <Link to="/roomconfiguration"><a className="nav-link" href="#">Room configuration</a></Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">House management</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">House monitoring (sensors)</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Energy consumption management</a>
                </li>
                <li className="nav-item">
                  <Link to="/home"><a className="nav-link" href="#">Log out<span className="sr-only">(current)</span></a></Link>
                </li>
              </ul>
            </div>
          </nav>


          <hr />

          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/home" component={Home} />
            <Route exact path="/houseconfiguration" component={Houseconfiguration} />
            <Route exact path="/roomconfiguration" component={Roomconfiguration} />
            <Route exact path="/us147" component={Us147} />
            <Route exact path="/topics" component={Topics} />
            <Route exact path="/topics/:id"  component={Topic}/>
            <Route component={NoMatch}/>
          </Switch>

        </Router>
      </div>
    );
  }
}

export default App;
